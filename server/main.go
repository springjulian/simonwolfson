package main

import (
	"log"

	"net/http"
	"encoding/json"
	"github.com/gorilla/websocket"
)

type Event struct {
	Id			 	int32 `json:"id"`
	Description 	string `json:"description`
	Heading 		string `json:"heading"`
}

var clients = make(map[*websocket.Conn]bool) // connected clients
var broadcast = make(chan Event)           // broadcast channel

// Configure the upgrader
var upgrader = websocket.Upgrader{}

 
func handleConnections(w http.ResponseWriter, r *http.Request) {
	log.Println("Handling new conection")
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Fatal(err)
	}
	// ensure connection close when function returns
	defer ws.Close()
	clients[ws] = true

	for {
		var msg Event
		// Read in a new message as JSON and map it to a Message object
		err := ws.ReadJSON(&msg)
		if err != nil {
			log.Printf("error: %v", err)
			delete(clients, ws)
			break
		}
		// send the new message to the broadcast channel
		broadcast <- msg
	}
}

func handleNewEvents(w http.ResponseWriter, r *http.Request) {
	resp, err := http.Get("http://localhost:1337/events")
	if err != nil {
		log.Printf("error: %v", err)
	}
	defer resp.Body.Close()
	var event []Event

	err = json.NewDecoder(resp.Body).Decode(&event)
	if err != nil {
        http.Error(w, err.Error(), http.StatusBadRequest)
        return
	}
	for _, data := range event {
		broadcast <- data
	}
}

func handleMessages() {
	for {
		// grab next message from the broadcast channel
		msg := <-broadcast
		// send it out to every client that is currently connected
		for client := range clients {
			log.Printf("%v", msg)
			err := client.WriteJSON(msg)
			if err != nil {
				log.Printf("error: %v", err)
				client.Close()
				delete(clients, client)
			}
		}
	}
}

func main() {
	http.HandleFunc("/ws", handleConnections)
	go handleMessages()

	http.HandleFunc("/eventsUpdated", handleNewEvents)	
	
	log.Println("http server started on :8000")
	err := http.ListenAndServe(":8000", nil)
	if err != nil {
		log.Fatal("ListAndServe: ", err)
	}
}