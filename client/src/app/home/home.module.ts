import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router'
import { CommonModule } from '@angular/common';
import {MatCardModule} from '@angular/material/card';
import { HomeComponent } from './home.component';
import {MatGridListModule} from '@angular/material/grid-list';

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    MatCardModule,
    MatGridListModule,   
    RouterModule,
  ],
  exports: [HomeComponent]
})
export class HomeModule { }
