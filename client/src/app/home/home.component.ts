import { Component, OnInit } from '@angular/core';

import { StrapiService, IEvent} from 'src/app/services/strapi-service.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  events: IEvent[] = [];
  private galleryUrl: string[] = [];

  constructor(private strapiService: StrapiService) { }

  ngOnInit() {
    this.getEvents();
    this.getGallery();
    //  this.strapiService.behaviourSub.subscribe((val: IEvent) => {
    //    console.log(val);
    //    this.le.unshift(val);
    //    console.log(this.le);
    //  })
  }
      
  getEvents(): void {
    this.strapiService.getEvents().subscribe((data: IEvent[]) => {
      this.events = data;
      console.log(this.events)
     });
  }

  get gallery(): string[] {
    return this.galleryUrl
  }

  private getGallery() {
    this.strapiService.getFeturedEvent().subscribe((data: any) => {
      data.gallery.forEach(element => {
        console.log(element.url);
        this.galleryUrl.push(element.url);
      });
     
    })
  }
}
