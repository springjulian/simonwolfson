import { TestBed } from '@angular/core/testing';

import { StrapiServiceService } from './strapi-service.service';

describe('StrapiServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StrapiServiceService = TestBed.get(StrapiServiceService);
    expect(service).toBeTruthy();
  });
});
