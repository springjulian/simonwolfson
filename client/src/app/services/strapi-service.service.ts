import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';
import { error } from 'selenium-webdriver';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

export interface IEvent {
  heading: string;
  description: string;
  summary: string;
  photoUrl: string;
  gallaryUrl: string[];
}

@Injectable({
  providedIn: 'root'
})
export class StrapiService {

  myWebSocket: WebSocketSubject<any> = webSocket('ws://localhost:8000/ws');

  behaviourSub = new BehaviorSubject<IEvent>({} as IEvent);

  private allEventsUrl = 'http://localhost:1337/events';
  private featuredEventUrl = 'http://localhost:1337/events/1';

  constructor(private httpClient: HttpClient) {
    this.myWebSocket.subscribe((event: IEvent) => {
      this.behaviourSub.next(event);
    },
      (error) => console.log(error)
    );
  }

  getEvents(): Observable<IEvent[]> {
    return this.httpClient.get<IEvent[]>(this.allEventsUrl);
  }

  getFeturedEvent(): Observable<IEvent> {
    return this.httpClient.get<IEvent>(this.featuredEventUrl);
  }

}
